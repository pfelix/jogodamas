﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jogodamas
{
    public partial class Form1 : Form
    {
        int turno = 0; // Vez de jogar

        bool movimentoExtra = false;

        PictureBox selecionado = null;
        List<PictureBox> brancas = new List<PictureBox>();
        List<PictureBox> pretas = new List<PictureBox>();

        TcpListener tcpListener; // Escuta num IP/porto
        TcpClient tcpCliente; // Liga, recebe, envia dados
        byte[] mRx; // Para guardar os dados enviados pelo cliente

        public Form1()
        {
            InitializeComponent();
            CarregarListas();
        }

        private void Movimento(PictureBox pictureBox)
        {
            if (selecionado !=null)
            {
                string cor = selecionado.Name.ToString().Substring(0, 5);

                if (Validacao(selecionado, pictureBox, cor))
                {
                    Point anterior = selecionado.Location;
                    selecionado.Location = pictureBox.Location;
                    int avancar = anterior.Y - pictureBox.Location.Y;

                    if (!movimentosExtras(cor) | Math.Abs(avancar) == 50)  //Verificar movimento extra
                    {
                        ifRainha(cor);
                        turno++;
                        EnviarDados(selecionado.Name
                            + ";"
                            + selecionado.Location.X
                            + ";"
                            + selecionado.Location.Y
                            + ";"
                            + selecionado.Tag
                            + ";"
                            + turno);
                        selecionado.BackColor = Color.Black;
                        selecionado = null;
                        movimentoExtra = false;
                    }
                    else
                    {
                        movimentoExtra = true;
                        EnviarDados(selecionado.Name 
                            + ";" 
                            + selecionado.Location.X 
                            + ";" 
                            + selecionado.Location.Y 
                            + ";"
                            + selecionado.Tag
                            + ";"
                            + turno);
                    }
                }
            }
        }

        private void EnviarDados(string dadosPeca)
        {
            byte[] sendTxt = new byte[1460];


            if (tcpCliente == null)
            {
                doInvoke("O cliente não está ligado...");
                return;
            }

            if (!tcpCliente.Client.Connected)
            {
                doInvoke("O cliente desligou-se...");
                return;
            }

            sendTxt = ASCIIEncoding.ASCII.GetBytes(dadosPeca);

            doInvoke(dadosPeca);

            tcpCliente.GetStream().BeginWrite(sendTxt, 0, sendTxt.Length, onCompleteWrite, tcpCliente); // Metodo que fica à espera para enviar dados para o cliente

        }

        private void onCompleteWrite(IAsyncResult iar)
        {
            TcpClient tcpc = (TcpClient)iar.AsyncState;
            tcpc.GetStream().EndWrite(iar);
        }

        private bool movimentosExtras(string cor)
        {
            List<PictureBox> ladoContratia = cor == "preta" ? brancas : pretas;

            List<Point> posicao = new List<Point>();

            int seguintePosicao = cor == "preta" ? -100 : 100;

            posicao.Add(new Point(selecionado.Location.X + 100, selecionado.Location.Y + seguintePosicao));
            posicao.Add(new Point(selecionado.Location.X - 100, selecionado.Location.Y - seguintePosicao));

            if (selecionado.Tag == "rainha")
            {
                posicao.Add(new Point(selecionado.Location.X + 100, selecionado.Location.Y + seguintePosicao));
                posicao.Add(new Point(selecionado.Location.X - 100, selecionado.Location.Y - seguintePosicao));
            }

            bool resultado = false;
            for (int i = 0; i < posicao.Count; i++)
            {
                if (posicao[i].X>=50 && posicao[i].X <=400 && posicao[i].Y >= 50 && posicao[i].Y <= 400)
                {
                    if (!ocupado(posicao[i], pretas) && !ocupado(posicao[i], brancas))
                    {
                        Point pontoMedio = new Point(pormeio(posicao[i].X, selecionado.Location.X), pormeio(posicao[i].Y, selecionado.Location.Y));

                        if (ocupado(pontoMedio, ladoContratia))
                        {
                            resultado = true;
                        }
                    }
                }
            }
            return resultado;
        }

        private bool ocupado (Point ponto, List<PictureBox> lado)
        {
            for ( int i = 0; i < lado.Count; i++)
            {
                if (ponto == lado[i].Location)
                {
                    return true;
                }
            }
            return false;
        }

        private int pormeio(int n1, int n2)
        {
            int resultado = n1 + n2;
            resultado = resultado / 2;
            return Math.Abs(resultado);
        }

        private bool Validacao(PictureBox origem, PictureBox destino, string cor)
        {
            Point pontoOrigem = origem.Location;
            Point pontoDestino = destino.Location;

            int avancar = pontoOrigem.Y - pontoDestino.Y;
            avancar = cor == "preta" ? avancar : (avancar * -1);
            avancar = selecionado.Tag == "rainha" ? Math.Abs(avancar) : avancar;
             
            if (avancar == 50)
            {
                return true;
            }
            else if (avancar == 100)
            {
                Point pontoMedio = new Point(pormeio(pontoDestino.X, pontoOrigem.X), pormeio(pontoDestino.Y, pontoOrigem.Y));

                List<PictureBox> corContraria = cor == "preta" ? brancas : pretas;

                for (int i = 0; i < corContraria.Count; i++)
                {
                    if (corContraria[i].Location == pontoMedio)
                    {
                        corContraria[i].Location = new Point(0, 0);
                        corContraria[i].Visible = false;
                        EnviarDados(corContraria[i].Name + ";0;0;"+corContraria[i].Tag);
                        return true;
                    }
                }
            }
            return false;
        }

        private void ifRainha(string cor)
        {
            if (cor == "branc" && selecionado.Location.Y == 400)
            {
                selecionado.BackgroundImage = Properties.Resources.damas_brancas__rainha;
                selecionado.Tag = "rainha";
            }
            else if (cor == "preta" && selecionado.Location.Y == 50)
            {
                selecionado.BackgroundImage = Properties.Resources.damas_pretas_rainha;
                selecionado.Tag = "rainha";
            }
        }

        private void CarregarListas()
        {
            brancas.Add(branco01);
            brancas.Add(branco02);
            brancas.Add(branco03);
            brancas.Add(branco04);
            brancas.Add(branco05);
            brancas.Add(branco06);
            brancas.Add(branco07);
            brancas.Add(branco08);
            brancas.Add(branco09);
            brancas.Add(branco10);
            brancas.Add(branco11);
            brancas.Add(branco12);

            pretas.Add(preta01);
            pretas.Add(preta02);
            pretas.Add(preta03);
            pretas.Add(preta04);
            pretas.Add(preta05);
            pretas.Add(preta06);
            pretas.Add(preta07);
            pretas.Add(preta08);
            pretas.Add(preta09);
            pretas.Add(preta10);
            pretas.Add(preta11);
            pretas.Add(preta12);
        }

        /// <summary>
        /// Metodo para mudar a cor de fundo quando está selecinado a ficha
        /// </summary>
        /// <param name="objeto"></param>
        public void Selecionado(object objeto)
        {
            if (!movimentoExtra)
            {
                try
                {
                    selecionado.BackColor = Color.Black;
                }
                catch
                {
                }

                PictureBox ficha = (PictureBox)objeto;

                selecionado = ficha;
                selecionado.BackColor = Color.Lime;
            }
        }

        private void caixaClick(object sender, MouseEventArgs e)
        {
            Movimento((PictureBox)sender);
        }

        /// <summary>
        /// Metodo associado ao eveto Mouse click das fichas pretas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selecionarPreta(object sender, MouseEventArgs e)
        {
            if (turno % 2 == 0)
            {
                Selecionado(sender);
            }
            else
            {
                MessageBox.Show("É a vez das fichas BRANCAS");
            }
            
        }

        /// <summary>
        /// Metodo associado ao eveto Mouse click das fichas brancas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void selecionarBranca(object sender, MouseEventArgs e)
        //{
        //    if (turno % 2 == 1)
        //    {
        //        Selecionado(sender);
        //    }
        //    else
        //    {
        //        MessageBox.Show("É a vez das fichas PRESTAS");
        //    }
        //}

        private void doInvoke(string texto)
        {
            TextBoxLog.Invoke((MethodInvoker)delegate { TextBoxLog.Text += texto + Environment.NewLine; }); ;
        }

        private void ButtonLigar_Click(object sender, EventArgs e)
        {
            IPAddress ipAddress; //Guarda IP do servidor
            int nport; // Guarda porto do server

            if (!IPAddress.TryParse(TextBoxIp.Text, out ipAddress))
            {
                doInvoke("Endereço IP inválido");
                return;
            }

            if (!int.TryParse(TextBoxPort.Text, out nport))
            {
                doInvoke("Porto inválido");
                return;
            }

            if (nport < 1024 || nport > 65535) // Port até 1024 são reservados pelo sistema operativo e 65535 é o máximo
            {
                doInvoke("Porto inválido");
                return;
            }

            tcpListener = new TcpListener(ipAddress, nport);
            tcpListener.Start(); // Coloca o servidor à escuta

            // Para ver os port que estão à escuta
            // netstat -an |find /i "Listening"

            // Para ver os port que estão ligados
            // netstat -an |find /i "established"

            // Para ver o estado do port
            // netstat -an |find /i "23000"

            doInvoke("Servidor à escuta no ip " + ipAddress + " e no porto " + nport);

            tcpListener.BeginAcceptTcpClient(onCompletAcceptClient, tcpListener); // Aceitar ou recusar a ligação. Manda a variável tcpListener para o metodo onCompletAcceptClient
        }

        private void onCompletAcceptClient(IAsyncResult iar)
        {
            TcpListener tcpListener = (TcpListener)iar.AsyncState; // Aceitamos a ligação

            tcpCliente = tcpListener.EndAcceptTcpClient(iar); // Guardamos o estado da ligação em mTcpCliente

            doInvoke("Cliente aceite... ");
            doInvoke("Cliente IP: " + ((IPEndPoint)tcpCliente.Client.RemoteEndPoint).Address);
            doInvoke("Porto: " + ((IPEndPoint)tcpCliente.Client.RemoteEndPoint).Port);

            //Receber os dados do cliente
            mRx = new byte[1460]; // o máximo que pode ser enviado num pacote é 1460 bytes. Estamos a criar espaço 
            tcpCliente.GetStream().BeginRead(mRx, 0, mRx.Length, onCompleteRead, tcpCliente); // Fica à espera de receber dados. Ler informação enviada pelo cliente. Manda a variável mTcpCliente para o metodo onCompleteRead
        }

        private void onCompleteRead(IAsyncResult iar)
        {
            TcpClient tcpc;

            int nReadBytes = 0; // var que quarda a qtd de bytes a lerdo pacote 

            string strRecv = null;

            tcpc = (TcpClient)iar.AsyncState; // cast para aceder aos dados enviados pelo cliente

            nReadBytes = tcpc.GetStream().EndRead(iar); // Quantos bytes vou ter do pacote que está a chegar do cliente

            strRecv = ASCIIEncoding.ASCII.GetString(mRx, 0, nReadBytes); // Le os dados recebidos

            // Carater de separação
            char separa = ';';

            // passar para um arreio a string, fazendo a separação por -
            // Na listbox o formado é idCliente - Nome
            string[] etiqueta = strRecv.Split(separa);

            if (etiqueta.Length > 3)
            {
                if (etiqueta.Length == 4)
                {
                    AlterarPossicaoRecebida(etiqueta, false); // Metodo para alterar os dados das peças que foram recebidas
                }
                else if (etiqueta.Length == 5)
                {
                    turno = Convert.ToInt32(etiqueta[4]);

                    AlterarPossicaoRecebida(etiqueta, true);
                }


                // Mostra dados recebidos 
                doInvoke(strRecv);


            }
            // Voltamos a meter para ficar à esperada de receber dados
            mRx = new byte[1460]; // o máximo que pode ser enviado num pacote é 1460 bytes. Estamos a criar espaço 
            tcpCliente.GetStream().BeginRead(mRx, 0, mRx.Length, onCompleteRead, tcpCliente); // Fica à espera de receber dados. Ler informação enviada pelo cliente. Manda a variável mTcpCliente para o metodo onCompleteRead
        }

        /// <summary>
        /// Metodo para utilizar na receção de dados para mudar a localização das peças
        /// </summary>
        private void AlterarPossicaoRecebida(string[] etiqueta, bool visible)
        {
            PictureBox destino = new PictureBox();

            List<Point> posicao = new List<Point>();

            int localicacaoX = Convert.ToInt32(etiqueta[1]);
            int localicacaoY = Convert.ToInt32(etiqueta[2]);

            if (etiqueta[0].Substring(0, 6) == "branco")
            {
                foreach (PictureBox item in brancas)
                {
                    if (item.Name == etiqueta[0])
                    {
                        item.Invoke((MethodInvoker)delegate
                        {
                            item.Location = new Point(localicacaoX, localicacaoY);
                            item.Visible = visible;
                            item.Tag = etiqueta[3];
                            if (etiqueta[3] != "")
                            {
                                item.Tag = etiqueta[3];
                                item.BackgroundImage = Properties.Resources.damas_brancas__rainha;
                            }
                        });

                        destino = item;

                        return;
                    }
                }
            }
            else
            {
                foreach (PictureBox item in pretas)
                {
                    if (item.Name == etiqueta[0])
                    {
                        item.Invoke((MethodInvoker)delegate
                        {
                            item.Location = new Point(localicacaoX, localicacaoY);
                            item.Visible = visible;
                            if (etiqueta[3] != "")
                            {
                                item.Tag = etiqueta[3];
                                item.BackgroundImage = Properties.Resources.damas_pretas_rainha;
                            }
                        });

                        destino = item;

                        return;
                    }
                }
            }
        }
    }
}
