﻿namespace jogodamas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.pictureBox61 = new System.Windows.Forms.PictureBox();
            this.pictureBox62 = new System.Windows.Forms.PictureBox();
            this.pictureBox63 = new System.Windows.Forms.PictureBox();
            this.pictureBox64 = new System.Windows.Forms.PictureBox();
            this.branco12 = new System.Windows.Forms.PictureBox();
            this.branco11 = new System.Windows.Forms.PictureBox();
            this.branco10 = new System.Windows.Forms.PictureBox();
            this.branco09 = new System.Windows.Forms.PictureBox();
            this.branco08 = new System.Windows.Forms.PictureBox();
            this.branco07 = new System.Windows.Forms.PictureBox();
            this.branco06 = new System.Windows.Forms.PictureBox();
            this.branco05 = new System.Windows.Forms.PictureBox();
            this.branco04 = new System.Windows.Forms.PictureBox();
            this.branco03 = new System.Windows.Forms.PictureBox();
            this.branco02 = new System.Windows.Forms.PictureBox();
            this.branco01 = new System.Windows.Forms.PictureBox();
            this.preta12 = new System.Windows.Forms.PictureBox();
            this.preta11 = new System.Windows.Forms.PictureBox();
            this.preta10 = new System.Windows.Forms.PictureBox();
            this.preta09 = new System.Windows.Forms.PictureBox();
            this.preta08 = new System.Windows.Forms.PictureBox();
            this.preta07 = new System.Windows.Forms.PictureBox();
            this.preta06 = new System.Windows.Forms.PictureBox();
            this.preta05 = new System.Windows.Forms.PictureBox();
            this.preta04 = new System.Windows.Forms.PictureBox();
            this.preta03 = new System.Windows.Forms.PictureBox();
            this.preta02 = new System.Windows.Forms.PictureBox();
            this.preta01 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxLog = new System.Windows.Forms.TextBox();
            this.ButtonLigar = new System.Windows.Forms.Button();
            this.TextBoxPort = new System.Windows.Forms.TextBox();
            this.TextBoxIp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta09)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta01)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Location = new System.Drawing.Point(50, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(100, 50);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 50);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Location = new System.Drawing.Point(200, 50);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Black;
            this.pictureBox4.Location = new System.Drawing.Point(150, 50);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.White;
            this.pictureBox5.Location = new System.Drawing.Point(400, 50);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 50);
            this.pictureBox5.TabIndex = 7;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Black;
            this.pictureBox6.Location = new System.Drawing.Point(350, 50);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(50, 50);
            this.pictureBox6.TabIndex = 6;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.White;
            this.pictureBox7.Location = new System.Drawing.Point(300, 50);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(50, 50);
            this.pictureBox7.TabIndex = 5;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Black;
            this.pictureBox8.Location = new System.Drawing.Point(250, 50);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(50, 50);
            this.pictureBox8.TabIndex = 4;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Black;
            this.pictureBox9.Location = new System.Drawing.Point(400, 100);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(50, 50);
            this.pictureBox9.TabIndex = 15;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.White;
            this.pictureBox10.Location = new System.Drawing.Point(350, 100);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(50, 50);
            this.pictureBox10.TabIndex = 14;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Black;
            this.pictureBox11.Location = new System.Drawing.Point(300, 100);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(50, 50);
            this.pictureBox11.TabIndex = 13;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.White;
            this.pictureBox12.Location = new System.Drawing.Point(250, 100);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(50, 50);
            this.pictureBox12.TabIndex = 12;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Black;
            this.pictureBox13.Location = new System.Drawing.Point(200, 100);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(50, 50);
            this.pictureBox13.TabIndex = 11;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.White;
            this.pictureBox14.Location = new System.Drawing.Point(150, 100);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(50, 50);
            this.pictureBox14.TabIndex = 10;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Black;
            this.pictureBox15.Location = new System.Drawing.Point(100, 100);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(50, 50);
            this.pictureBox15.TabIndex = 9;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.White;
            this.pictureBox16.Location = new System.Drawing.Point(50, 100);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(50, 50);
            this.pictureBox16.TabIndex = 8;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Black;
            this.pictureBox17.Location = new System.Drawing.Point(400, 200);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(50, 50);
            this.pictureBox17.TabIndex = 31;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.White;
            this.pictureBox18.Location = new System.Drawing.Point(350, 200);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(50, 50);
            this.pictureBox18.TabIndex = 30;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.Black;
            this.pictureBox19.Location = new System.Drawing.Point(300, 200);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(50, 50);
            this.pictureBox19.TabIndex = 29;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.White;
            this.pictureBox20.Location = new System.Drawing.Point(250, 200);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(50, 50);
            this.pictureBox20.TabIndex = 28;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Black;
            this.pictureBox21.Location = new System.Drawing.Point(200, 200);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(50, 50);
            this.pictureBox21.TabIndex = 27;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.White;
            this.pictureBox22.Location = new System.Drawing.Point(150, 200);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(50, 50);
            this.pictureBox22.TabIndex = 26;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.Black;
            this.pictureBox23.Location = new System.Drawing.Point(100, 200);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(50, 50);
            this.pictureBox23.TabIndex = 25;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.White;
            this.pictureBox24.Location = new System.Drawing.Point(50, 200);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(50, 50);
            this.pictureBox24.TabIndex = 24;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.White;
            this.pictureBox25.Location = new System.Drawing.Point(400, 150);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(50, 50);
            this.pictureBox25.TabIndex = 23;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.Black;
            this.pictureBox26.Location = new System.Drawing.Point(350, 150);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(50, 50);
            this.pictureBox26.TabIndex = 22;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.Color.White;
            this.pictureBox27.Location = new System.Drawing.Point(300, 150);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(50, 50);
            this.pictureBox27.TabIndex = 21;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.Color.Black;
            this.pictureBox28.Location = new System.Drawing.Point(250, 150);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(50, 50);
            this.pictureBox28.TabIndex = 20;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.Color.White;
            this.pictureBox29.Location = new System.Drawing.Point(200, 150);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(50, 50);
            this.pictureBox29.TabIndex = 19;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.Black;
            this.pictureBox30.Location = new System.Drawing.Point(150, 150);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(50, 50);
            this.pictureBox30.TabIndex = 18;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.White;
            this.pictureBox31.Location = new System.Drawing.Point(100, 150);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(50, 50);
            this.pictureBox31.TabIndex = 17;
            this.pictureBox31.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.Color.Black;
            this.pictureBox32.Location = new System.Drawing.Point(50, 150);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(50, 50);
            this.pictureBox32.TabIndex = 16;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.Color.Black;
            this.pictureBox33.Location = new System.Drawing.Point(400, 400);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(50, 50);
            this.pictureBox33.TabIndex = 63;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackColor = System.Drawing.Color.White;
            this.pictureBox34.Location = new System.Drawing.Point(350, 400);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(50, 50);
            this.pictureBox34.TabIndex = 62;
            this.pictureBox34.TabStop = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackColor = System.Drawing.Color.Black;
            this.pictureBox35.Location = new System.Drawing.Point(300, 400);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(50, 50);
            this.pictureBox35.TabIndex = 61;
            this.pictureBox35.TabStop = false;
            this.pictureBox35.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackColor = System.Drawing.Color.White;
            this.pictureBox36.Location = new System.Drawing.Point(250, 400);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(50, 50);
            this.pictureBox36.TabIndex = 60;
            this.pictureBox36.TabStop = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackColor = System.Drawing.Color.Black;
            this.pictureBox37.Location = new System.Drawing.Point(200, 400);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(50, 50);
            this.pictureBox37.TabIndex = 59;
            this.pictureBox37.TabStop = false;
            this.pictureBox37.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackColor = System.Drawing.Color.White;
            this.pictureBox38.Location = new System.Drawing.Point(150, 400);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(50, 50);
            this.pictureBox38.TabIndex = 58;
            this.pictureBox38.TabStop = false;
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackColor = System.Drawing.Color.Black;
            this.pictureBox39.Location = new System.Drawing.Point(100, 400);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(50, 50);
            this.pictureBox39.TabIndex = 57;
            this.pictureBox39.TabStop = false;
            this.pictureBox39.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox40
            // 
            this.pictureBox40.BackColor = System.Drawing.Color.White;
            this.pictureBox40.Location = new System.Drawing.Point(50, 400);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(50, 50);
            this.pictureBox40.TabIndex = 56;
            this.pictureBox40.TabStop = false;
            // 
            // pictureBox41
            // 
            this.pictureBox41.BackColor = System.Drawing.Color.White;
            this.pictureBox41.Location = new System.Drawing.Point(400, 350);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(50, 50);
            this.pictureBox41.TabIndex = 55;
            this.pictureBox41.TabStop = false;
            // 
            // pictureBox42
            // 
            this.pictureBox42.BackColor = System.Drawing.Color.Black;
            this.pictureBox42.Location = new System.Drawing.Point(350, 350);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(50, 50);
            this.pictureBox42.TabIndex = 54;
            this.pictureBox42.TabStop = false;
            this.pictureBox42.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox43
            // 
            this.pictureBox43.BackColor = System.Drawing.Color.White;
            this.pictureBox43.Location = new System.Drawing.Point(300, 350);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(50, 50);
            this.pictureBox43.TabIndex = 53;
            this.pictureBox43.TabStop = false;
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackColor = System.Drawing.Color.Black;
            this.pictureBox44.Location = new System.Drawing.Point(250, 350);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(50, 50);
            this.pictureBox44.TabIndex = 52;
            this.pictureBox44.TabStop = false;
            this.pictureBox44.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox45
            // 
            this.pictureBox45.BackColor = System.Drawing.Color.White;
            this.pictureBox45.Location = new System.Drawing.Point(200, 350);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(50, 50);
            this.pictureBox45.TabIndex = 51;
            this.pictureBox45.TabStop = false;
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackColor = System.Drawing.Color.Black;
            this.pictureBox46.Location = new System.Drawing.Point(150, 350);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(50, 50);
            this.pictureBox46.TabIndex = 50;
            this.pictureBox46.TabStop = false;
            this.pictureBox46.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackColor = System.Drawing.Color.White;
            this.pictureBox47.Location = new System.Drawing.Point(100, 350);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(50, 50);
            this.pictureBox47.TabIndex = 49;
            this.pictureBox47.TabStop = false;
            // 
            // pictureBox48
            // 
            this.pictureBox48.BackColor = System.Drawing.Color.Black;
            this.pictureBox48.Location = new System.Drawing.Point(50, 350);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(50, 50);
            this.pictureBox48.TabIndex = 48;
            this.pictureBox48.TabStop = false;
            this.pictureBox48.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox49
            // 
            this.pictureBox49.BackColor = System.Drawing.Color.Black;
            this.pictureBox49.Location = new System.Drawing.Point(400, 300);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(50, 50);
            this.pictureBox49.TabIndex = 47;
            this.pictureBox49.TabStop = false;
            this.pictureBox49.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox50
            // 
            this.pictureBox50.BackColor = System.Drawing.Color.White;
            this.pictureBox50.Location = new System.Drawing.Point(350, 300);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(50, 50);
            this.pictureBox50.TabIndex = 46;
            this.pictureBox50.TabStop = false;
            // 
            // pictureBox51
            // 
            this.pictureBox51.BackColor = System.Drawing.Color.Black;
            this.pictureBox51.Location = new System.Drawing.Point(300, 300);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(50, 50);
            this.pictureBox51.TabIndex = 45;
            this.pictureBox51.TabStop = false;
            this.pictureBox51.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackColor = System.Drawing.Color.White;
            this.pictureBox52.Location = new System.Drawing.Point(250, 300);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(50, 50);
            this.pictureBox52.TabIndex = 44;
            this.pictureBox52.TabStop = false;
            // 
            // pictureBox53
            // 
            this.pictureBox53.BackColor = System.Drawing.Color.Black;
            this.pictureBox53.Location = new System.Drawing.Point(200, 300);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(50, 50);
            this.pictureBox53.TabIndex = 43;
            this.pictureBox53.TabStop = false;
            this.pictureBox53.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox54
            // 
            this.pictureBox54.BackColor = System.Drawing.Color.White;
            this.pictureBox54.Location = new System.Drawing.Point(150, 300);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(50, 50);
            this.pictureBox54.TabIndex = 42;
            this.pictureBox54.TabStop = false;
            // 
            // pictureBox55
            // 
            this.pictureBox55.BackColor = System.Drawing.Color.Black;
            this.pictureBox55.Location = new System.Drawing.Point(100, 300);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(50, 50);
            this.pictureBox55.TabIndex = 41;
            this.pictureBox55.TabStop = false;
            this.pictureBox55.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox56
            // 
            this.pictureBox56.BackColor = System.Drawing.Color.White;
            this.pictureBox56.Location = new System.Drawing.Point(50, 300);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(50, 50);
            this.pictureBox56.TabIndex = 40;
            this.pictureBox56.TabStop = false;
            // 
            // pictureBox57
            // 
            this.pictureBox57.BackColor = System.Drawing.Color.White;
            this.pictureBox57.Location = new System.Drawing.Point(400, 250);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(50, 50);
            this.pictureBox57.TabIndex = 39;
            this.pictureBox57.TabStop = false;
            // 
            // pictureBox58
            // 
            this.pictureBox58.BackColor = System.Drawing.Color.Black;
            this.pictureBox58.Location = new System.Drawing.Point(350, 250);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(50, 50);
            this.pictureBox58.TabIndex = 38;
            this.pictureBox58.TabStop = false;
            this.pictureBox58.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox59
            // 
            this.pictureBox59.BackColor = System.Drawing.Color.White;
            this.pictureBox59.Location = new System.Drawing.Point(300, 250);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(50, 50);
            this.pictureBox59.TabIndex = 37;
            this.pictureBox59.TabStop = false;
            // 
            // pictureBox60
            // 
            this.pictureBox60.BackColor = System.Drawing.Color.Black;
            this.pictureBox60.Location = new System.Drawing.Point(250, 250);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(50, 50);
            this.pictureBox60.TabIndex = 36;
            this.pictureBox60.TabStop = false;
            this.pictureBox60.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox61
            // 
            this.pictureBox61.BackColor = System.Drawing.Color.White;
            this.pictureBox61.Location = new System.Drawing.Point(200, 250);
            this.pictureBox61.Name = "pictureBox61";
            this.pictureBox61.Size = new System.Drawing.Size(50, 50);
            this.pictureBox61.TabIndex = 35;
            this.pictureBox61.TabStop = false;
            // 
            // pictureBox62
            // 
            this.pictureBox62.BackColor = System.Drawing.Color.Black;
            this.pictureBox62.Location = new System.Drawing.Point(150, 250);
            this.pictureBox62.Name = "pictureBox62";
            this.pictureBox62.Size = new System.Drawing.Size(50, 50);
            this.pictureBox62.TabIndex = 34;
            this.pictureBox62.TabStop = false;
            this.pictureBox62.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // pictureBox63
            // 
            this.pictureBox63.BackColor = System.Drawing.Color.White;
            this.pictureBox63.Location = new System.Drawing.Point(100, 250);
            this.pictureBox63.Name = "pictureBox63";
            this.pictureBox63.Size = new System.Drawing.Size(50, 50);
            this.pictureBox63.TabIndex = 33;
            this.pictureBox63.TabStop = false;
            // 
            // pictureBox64
            // 
            this.pictureBox64.BackColor = System.Drawing.Color.Black;
            this.pictureBox64.Location = new System.Drawing.Point(50, 250);
            this.pictureBox64.Name = "pictureBox64";
            this.pictureBox64.Size = new System.Drawing.Size(50, 50);
            this.pictureBox64.TabIndex = 32;
            this.pictureBox64.TabStop = false;
            this.pictureBox64.MouseClick += new System.Windows.Forms.MouseEventHandler(this.caixaClick);
            // 
            // branco12
            // 
            this.branco12.BackColor = System.Drawing.Color.Black;
            this.branco12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco12.BackgroundImage")));
            this.branco12.Location = new System.Drawing.Point(350, 150);
            this.branco12.Name = "branco12";
            this.branco12.Size = new System.Drawing.Size(50, 50);
            this.branco12.TabIndex = 75;
            this.branco12.TabStop = false;
            // 
            // branco11
            // 
            this.branco11.BackColor = System.Drawing.Color.Black;
            this.branco11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco11.BackgroundImage")));
            this.branco11.Location = new System.Drawing.Point(250, 150);
            this.branco11.Name = "branco11";
            this.branco11.Size = new System.Drawing.Size(50, 50);
            this.branco11.TabIndex = 74;
            this.branco11.TabStop = false;
            // 
            // branco10
            // 
            this.branco10.BackColor = System.Drawing.Color.Black;
            this.branco10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco10.BackgroundImage")));
            this.branco10.Location = new System.Drawing.Point(150, 150);
            this.branco10.Name = "branco10";
            this.branco10.Size = new System.Drawing.Size(50, 50);
            this.branco10.TabIndex = 73;
            this.branco10.TabStop = false;
            // 
            // branco09
            // 
            this.branco09.BackColor = System.Drawing.Color.Black;
            this.branco09.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco09.BackgroundImage")));
            this.branco09.Location = new System.Drawing.Point(50, 150);
            this.branco09.Name = "branco09";
            this.branco09.Size = new System.Drawing.Size(50, 50);
            this.branco09.TabIndex = 72;
            this.branco09.TabStop = false;
            // 
            // branco08
            // 
            this.branco08.BackColor = System.Drawing.Color.Black;
            this.branco08.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco08.BackgroundImage")));
            this.branco08.Location = new System.Drawing.Point(400, 100);
            this.branco08.Name = "branco08";
            this.branco08.Size = new System.Drawing.Size(50, 50);
            this.branco08.TabIndex = 71;
            this.branco08.TabStop = false;
            // 
            // branco07
            // 
            this.branco07.BackColor = System.Drawing.Color.Black;
            this.branco07.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco07.BackgroundImage")));
            this.branco07.Location = new System.Drawing.Point(300, 100);
            this.branco07.Name = "branco07";
            this.branco07.Size = new System.Drawing.Size(50, 50);
            this.branco07.TabIndex = 70;
            this.branco07.TabStop = false;
            // 
            // branco06
            // 
            this.branco06.BackColor = System.Drawing.Color.Black;
            this.branco06.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco06.BackgroundImage")));
            this.branco06.Location = new System.Drawing.Point(200, 100);
            this.branco06.Name = "branco06";
            this.branco06.Size = new System.Drawing.Size(50, 50);
            this.branco06.TabIndex = 69;
            this.branco06.TabStop = false;
            // 
            // branco05
            // 
            this.branco05.BackColor = System.Drawing.Color.Black;
            this.branco05.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco05.BackgroundImage")));
            this.branco05.Location = new System.Drawing.Point(100, 100);
            this.branco05.Name = "branco05";
            this.branco05.Size = new System.Drawing.Size(50, 50);
            this.branco05.TabIndex = 68;
            this.branco05.TabStop = false;
            // 
            // branco04
            // 
            this.branco04.BackColor = System.Drawing.Color.Black;
            this.branco04.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco04.BackgroundImage")));
            this.branco04.Location = new System.Drawing.Point(350, 50);
            this.branco04.Name = "branco04";
            this.branco04.Size = new System.Drawing.Size(50, 50);
            this.branco04.TabIndex = 67;
            this.branco04.TabStop = false;
            // 
            // branco03
            // 
            this.branco03.BackColor = System.Drawing.Color.Black;
            this.branco03.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco03.BackgroundImage")));
            this.branco03.Location = new System.Drawing.Point(250, 50);
            this.branco03.Name = "branco03";
            this.branco03.Size = new System.Drawing.Size(50, 50);
            this.branco03.TabIndex = 66;
            this.branco03.TabStop = false;
            // 
            // branco02
            // 
            this.branco02.BackColor = System.Drawing.Color.Black;
            this.branco02.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco02.BackgroundImage")));
            this.branco02.Location = new System.Drawing.Point(150, 50);
            this.branco02.Name = "branco02";
            this.branco02.Size = new System.Drawing.Size(50, 50);
            this.branco02.TabIndex = 65;
            this.branco02.TabStop = false;
            // 
            // branco01
            // 
            this.branco01.BackColor = System.Drawing.Color.Black;
            this.branco01.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("branco01.BackgroundImage")));
            this.branco01.Location = new System.Drawing.Point(50, 50);
            this.branco01.Name = "branco01";
            this.branco01.Size = new System.Drawing.Size(50, 50);
            this.branco01.TabIndex = 64;
            this.branco01.TabStop = false;
            // 
            // preta12
            // 
            this.preta12.BackColor = System.Drawing.Color.Black;
            this.preta12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta12.BackgroundImage")));
            this.preta12.Location = new System.Drawing.Point(400, 400);
            this.preta12.Name = "preta12";
            this.preta12.Size = new System.Drawing.Size(50, 50);
            this.preta12.TabIndex = 87;
            this.preta12.TabStop = false;
            this.preta12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta11
            // 
            this.preta11.BackColor = System.Drawing.Color.Black;
            this.preta11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta11.BackgroundImage")));
            this.preta11.Location = new System.Drawing.Point(300, 400);
            this.preta11.Name = "preta11";
            this.preta11.Size = new System.Drawing.Size(50, 50);
            this.preta11.TabIndex = 86;
            this.preta11.TabStop = false;
            this.preta11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta10
            // 
            this.preta10.BackColor = System.Drawing.Color.Black;
            this.preta10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta10.BackgroundImage")));
            this.preta10.Location = new System.Drawing.Point(200, 400);
            this.preta10.Name = "preta10";
            this.preta10.Size = new System.Drawing.Size(50, 50);
            this.preta10.TabIndex = 85;
            this.preta10.TabStop = false;
            this.preta10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta09
            // 
            this.preta09.BackColor = System.Drawing.Color.Black;
            this.preta09.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta09.BackgroundImage")));
            this.preta09.Location = new System.Drawing.Point(100, 400);
            this.preta09.Name = "preta09";
            this.preta09.Size = new System.Drawing.Size(50, 50);
            this.preta09.TabIndex = 84;
            this.preta09.TabStop = false;
            this.preta09.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta08
            // 
            this.preta08.BackColor = System.Drawing.Color.Black;
            this.preta08.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta08.BackgroundImage")));
            this.preta08.Location = new System.Drawing.Point(50, 350);
            this.preta08.Name = "preta08";
            this.preta08.Size = new System.Drawing.Size(50, 50);
            this.preta08.TabIndex = 83;
            this.preta08.TabStop = false;
            this.preta08.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta07
            // 
            this.preta07.BackColor = System.Drawing.Color.Black;
            this.preta07.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta07.BackgroundImage")));
            this.preta07.Location = new System.Drawing.Point(350, 350);
            this.preta07.Name = "preta07";
            this.preta07.Size = new System.Drawing.Size(50, 50);
            this.preta07.TabIndex = 82;
            this.preta07.TabStop = false;
            this.preta07.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta06
            // 
            this.preta06.BackColor = System.Drawing.Color.Black;
            this.preta06.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta06.BackgroundImage")));
            this.preta06.Location = new System.Drawing.Point(250, 350);
            this.preta06.Name = "preta06";
            this.preta06.Size = new System.Drawing.Size(50, 50);
            this.preta06.TabIndex = 81;
            this.preta06.TabStop = false;
            this.preta06.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta05
            // 
            this.preta05.BackColor = System.Drawing.Color.Black;
            this.preta05.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta05.BackgroundImage")));
            this.preta05.Location = new System.Drawing.Point(150, 350);
            this.preta05.Name = "preta05";
            this.preta05.Size = new System.Drawing.Size(50, 50);
            this.preta05.TabIndex = 80;
            this.preta05.TabStop = false;
            this.preta05.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta04
            // 
            this.preta04.BackColor = System.Drawing.Color.Black;
            this.preta04.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta04.BackgroundImage")));
            this.preta04.Location = new System.Drawing.Point(400, 300);
            this.preta04.Name = "preta04";
            this.preta04.Size = new System.Drawing.Size(50, 50);
            this.preta04.TabIndex = 79;
            this.preta04.TabStop = false;
            this.preta04.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta03
            // 
            this.preta03.BackColor = System.Drawing.Color.Black;
            this.preta03.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta03.BackgroundImage")));
            this.preta03.Location = new System.Drawing.Point(300, 300);
            this.preta03.Name = "preta03";
            this.preta03.Size = new System.Drawing.Size(50, 50);
            this.preta03.TabIndex = 78;
            this.preta03.TabStop = false;
            this.preta03.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta02
            // 
            this.preta02.BackColor = System.Drawing.Color.Black;
            this.preta02.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta02.BackgroundImage")));
            this.preta02.Location = new System.Drawing.Point(200, 300);
            this.preta02.Name = "preta02";
            this.preta02.Size = new System.Drawing.Size(50, 50);
            this.preta02.TabIndex = 77;
            this.preta02.TabStop = false;
            this.preta02.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // preta01
            // 
            this.preta01.BackColor = System.Drawing.Color.Black;
            this.preta01.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("preta01.BackgroundImage")));
            this.preta01.Location = new System.Drawing.Point(100, 300);
            this.preta01.Name = "preta01";
            this.preta01.Size = new System.Drawing.Size(50, 50);
            this.preta01.TabIndex = 76;
            this.preta01.TabStop = false;
            this.preta01.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selecionarPreta);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TextBoxLog);
            this.groupBox1.Controls.Add(this.ButtonLigar);
            this.groupBox1.Controls.Add(this.TextBoxPort);
            this.groupBox1.Controls.Add(this.TextBoxIp);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(514, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 210);
            this.groupBox1.TabIndex = 89;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Servidor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Log";
            // 
            // TextBoxLog
            // 
            this.TextBoxLog.Location = new System.Drawing.Point(9, 109);
            this.TextBoxLog.Multiline = true;
            this.TextBoxLog.Name = "TextBoxLog";
            this.TextBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxLog.Size = new System.Drawing.Size(343, 86);
            this.TextBoxLog.TabIndex = 5;
            // 
            // ButtonLigar
            // 
            this.ButtonLigar.Location = new System.Drawing.Point(277, 22);
            this.ButtonLigar.Name = "ButtonLigar";
            this.ButtonLigar.Size = new System.Drawing.Size(75, 62);
            this.ButtonLigar.TabIndex = 4;
            this.ButtonLigar.Text = "Ligar...";
            this.ButtonLigar.UseVisualStyleBackColor = true;
            this.ButtonLigar.Click += new System.EventHandler(this.ButtonLigar_Click);
            // 
            // TextBoxPort
            // 
            this.TextBoxPort.Location = new System.Drawing.Point(62, 64);
            this.TextBoxPort.Name = "TextBoxPort";
            this.TextBoxPort.Size = new System.Drawing.Size(100, 20);
            this.TextBoxPort.TabIndex = 3;
            this.TextBoxPort.Text = "23000";
            // 
            // TextBoxIp
            // 
            this.TextBoxIp.Location = new System.Drawing.Point(61, 22);
            this.TextBoxIp.Name = "TextBoxIp";
            this.TextBoxIp.Size = new System.Drawing.Size(168, 20);
            this.TextBoxIp.TabIndex = 2;
            this.TextBoxIp.Text = "10.2.3.14";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 477);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.preta12);
            this.Controls.Add(this.preta11);
            this.Controls.Add(this.preta10);
            this.Controls.Add(this.preta09);
            this.Controls.Add(this.preta08);
            this.Controls.Add(this.preta07);
            this.Controls.Add(this.preta06);
            this.Controls.Add(this.preta05);
            this.Controls.Add(this.preta04);
            this.Controls.Add(this.preta03);
            this.Controls.Add(this.preta02);
            this.Controls.Add(this.preta01);
            this.Controls.Add(this.branco12);
            this.Controls.Add(this.branco11);
            this.Controls.Add(this.branco10);
            this.Controls.Add(this.branco09);
            this.Controls.Add(this.branco08);
            this.Controls.Add(this.branco07);
            this.Controls.Add(this.branco06);
            this.Controls.Add(this.branco05);
            this.Controls.Add(this.branco04);
            this.Controls.Add(this.branco03);
            this.Controls.Add(this.branco02);
            this.Controls.Add(this.branco01);
            this.Controls.Add(this.pictureBox33);
            this.Controls.Add(this.pictureBox34);
            this.Controls.Add(this.pictureBox35);
            this.Controls.Add(this.pictureBox36);
            this.Controls.Add(this.pictureBox37);
            this.Controls.Add(this.pictureBox38);
            this.Controls.Add(this.pictureBox39);
            this.Controls.Add(this.pictureBox40);
            this.Controls.Add(this.pictureBox41);
            this.Controls.Add(this.pictureBox42);
            this.Controls.Add(this.pictureBox43);
            this.Controls.Add(this.pictureBox44);
            this.Controls.Add(this.pictureBox45);
            this.Controls.Add(this.pictureBox46);
            this.Controls.Add(this.pictureBox47);
            this.Controls.Add(this.pictureBox48);
            this.Controls.Add(this.pictureBox49);
            this.Controls.Add(this.pictureBox50);
            this.Controls.Add(this.pictureBox51);
            this.Controls.Add(this.pictureBox52);
            this.Controls.Add(this.pictureBox53);
            this.Controls.Add(this.pictureBox54);
            this.Controls.Add(this.pictureBox55);
            this.Controls.Add(this.pictureBox56);
            this.Controls.Add(this.pictureBox57);
            this.Controls.Add(this.pictureBox58);
            this.Controls.Add(this.pictureBox59);
            this.Controls.Add(this.pictureBox60);
            this.Controls.Add(this.pictureBox61);
            this.Controls.Add(this.pictureBox62);
            this.Controls.Add(this.pictureBox63);
            this.Controls.Add(this.pictureBox64);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.pictureBox24);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.pictureBox28);
            this.Controls.Add(this.pictureBox29);
            this.Controls.Add(this.pictureBox30);
            this.Controls.Add(this.pictureBox31);
            this.Controls.Add(this.pictureBox32);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Jogo das damas - Servidor";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branco01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta09)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preta01)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.PictureBox pictureBox55;
        private System.Windows.Forms.PictureBox pictureBox56;
        private System.Windows.Forms.PictureBox pictureBox57;
        private System.Windows.Forms.PictureBox pictureBox58;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.PictureBox pictureBox61;
        private System.Windows.Forms.PictureBox pictureBox62;
        private System.Windows.Forms.PictureBox pictureBox63;
        private System.Windows.Forms.PictureBox pictureBox64;
        private System.Windows.Forms.PictureBox branco12;
        private System.Windows.Forms.PictureBox branco11;
        private System.Windows.Forms.PictureBox branco10;
        private System.Windows.Forms.PictureBox branco09;
        private System.Windows.Forms.PictureBox branco08;
        private System.Windows.Forms.PictureBox branco07;
        private System.Windows.Forms.PictureBox branco06;
        private System.Windows.Forms.PictureBox branco05;
        private System.Windows.Forms.PictureBox branco04;
        private System.Windows.Forms.PictureBox branco03;
        private System.Windows.Forms.PictureBox branco02;
        private System.Windows.Forms.PictureBox branco01;
        private System.Windows.Forms.PictureBox preta12;
        private System.Windows.Forms.PictureBox preta11;
        private System.Windows.Forms.PictureBox preta10;
        private System.Windows.Forms.PictureBox preta09;
        private System.Windows.Forms.PictureBox preta08;
        private System.Windows.Forms.PictureBox preta07;
        private System.Windows.Forms.PictureBox preta06;
        private System.Windows.Forms.PictureBox preta05;
        private System.Windows.Forms.PictureBox preta04;
        private System.Windows.Forms.PictureBox preta03;
        private System.Windows.Forms.PictureBox preta02;
        private System.Windows.Forms.PictureBox preta01;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ButtonLigar;
        private System.Windows.Forms.TextBox TextBoxPort;
        private System.Windows.Forms.TextBox TextBoxIp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxLog;
    }
}

